package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;

public class ClubTest {
	
	@Test
	public void testNumberOf()
	{
		Club a = new Club();
		
		int erg = a.numberOf();
		int erwartet = 0;
		assertEquals(erwartet, erg);
	}
	
	@Test
	public void testDurchschnittsalter()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Flora", "Bond", LocalDate.parse("1970-10-29"), Person.Sex.FEMALE);
		a.enter(p);
		a.enter(p2);
		int erg = a.durchschnittsalter();
		int erwartet = 54;
		assertEquals(erwartet, erg);
	}
	
	@Test
	public void testAustreten()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Flora", "Bond", LocalDate.parse("1970-10-29"), Person.Sex.FEMALE);
		Person p3 = new Person("Alex", "Bond", LocalDate.parse("1995-06-12"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		
		a.austreten(p3);
		
		
		int erg = a.getPersons().size();
		int erwartet = 2;
		assertEquals(erwartet, erg);
	}
	
	@Test
	public void testSort()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Flora", "Bond", LocalDate.parse("1970-10-29"), Person.Sex.FEMALE);
		Person p3 = new Person("Alex", "Bond", LocalDate.parse("1995-06-12"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		
		a.sortieren();
	}
	
	@Test
	public void testaendernName()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Flora", "Bond", LocalDate.parse("1970-10-29"), Person.Sex.FEMALE);
		Person p3 = new Person("Alex", "Bond", LocalDate.parse("1995-06-12"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		
		String erwartet = "Bob";
		
		a.aenderName("Alex", "Bond", "Bob", "LOL");
		assertSame(erwartet,a.getPersons().get(2).getFirstName());
		
	}
	
	@Test
	public void testaendernMALE()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Flora", "Bond", LocalDate.parse("1970-10-29"), Person.Sex.FEMALE);
		Person p3 = new Person("Alex", "Bond", LocalDate.parse("1995-06-12"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		
		String erwartet = "MALE";
		
		a.setMale("Flora", "Bond");
		assertSame(erwartet,a.getPersons().get(1).getSex().toString());
	}
	
	@Test
	public void testSortName()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Person p2 = new Person("Flora", "Blond", LocalDate.parse("1970-10-29"), Person.Sex.FEMALE);
		Person p3 = new Person("Alex", "Blondi", LocalDate.parse("1995-06-12"), Person.Sex.MALE);
		Person p4 = new Person("Alex", "Ahorn", LocalDate.parse("1995-06-12"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		a.enter(p4);
		
		a.sortName();
		
		List<Person> l = a.getPersons();
		assertTrue(l.get(0).getLastName().equals("Ahorn"));
		assertTrue(l.get(1).getLastName().equals("Blond"));
		assertTrue(l.get(2).getLastName().equals("Blondi"));
		assertTrue(l.get(3).getLastName().equals("Bond"));
	}
	
	@Test
	public void testSortAlter()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);//64
		Person p2 = new Person("Flora", "Blond", LocalDate.parse("1970-10-29"), Person.Sex.FEMALE);//44
		Person p3 = new Person("Alex", "Blondi", LocalDate.parse("1995-06-12"), Person.Sex.MALE);//19
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		
		a.sortAlter();
		
		List<Person> l = a.getPersons();
		assertTrue(l.get(0).getLastName().equals("Blondi"));
		assertTrue(l.get(1).getLastName().equals("Blond"));
		assertTrue(l.get(2).getLastName().equals("Bond"));
	}
	
	@Test
	public void testGroupAlter()
	{
		Club a = new Club();
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);//64
		Person p2 = new Person("Flora", "Blond", LocalDate.parse("1970-10-29"), Person.Sex.FEMALE);//44
		Person p3 = new Person("Alex", "Blondi", LocalDate.parse("1995-06-12"), Person.Sex.MALE);//19
		Person p4 = new Person("Alex", "Ahorn", LocalDate.parse("1995-06-12"), Person.Sex.MALE);
		a.enter(p);
		a.enter(p2);
		a.enter(p3);
		a.enter(p4);
		

		assertEquals("{64=1, 19=2, 44=1}",a.groupAlter().toString());
	}
	
	
	
	
}
