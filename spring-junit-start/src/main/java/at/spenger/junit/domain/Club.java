package at.spenger.junit.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import at.spenger.junit.domain.Person.Sex;

public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}
	
	public void sortieren()
	{
		  Collections.sort(l, new PersonComparator());	  
	}

	
	public void austreten(Person p)
	{
		l.remove(p);
	}
	
	public class PersonComparator implements Comparator<Person>
	{
	     @Override
	     public int compare(Person o1, Person o2) {
	         return o1.getFirstName().compareTo(o2.getFirstName());
	     }
	 }
	
	public void aenderName(String vn, String nn, String nvn, String nnn)
	{
		for(int i = 0;i<l.size();i++)
		{
			if(l.get(i).getFirstName().equalsIgnoreCase(vn)&&l.get(i).getLastName().equalsIgnoreCase(nn))
			{
				l.get(i).setFirstName(nvn);
				l.get(i).setLastName(nnn);
			}
		}
	}
	
	public void setMale(String vn, String nn)
	{
		for(int i = 0;i<l.size();i++)
		{
			if(l.get(i).getFirstName().equalsIgnoreCase(vn)&&l.get(i).getLastName().equalsIgnoreCase(nn))
			{
				l.get(i).setSex(Sex.MALE);
			}
		}
	}
	
	public int durchschnittsalter()
	{
		int d = 0;
		for(int i = 0;i<l.size();i++)
		{
				d = d + l.get(i).getAge();
		}
		return d/l.size();
	}
	
	public void sortName()
	 {
	  l = l.stream()
	    .sorted((x, y) -> x.getLastName().compareTo(y.getLastName()))
	    .collect(Collectors.toList());
	 }
	 
	 public void sortAlter()
	 {
	  l = l.stream()
	    .sorted((a, b) -> Integer.compare(a.getAge(), b.getAge()))
	    .collect(Collectors.toList());
	 }
	 
	 public Map<Integer, Long> groupAlter()
	 {
	  Map<Integer, Long> ma = l.stream()
	    .collect(Collectors.groupingBy(Person::getAge, Collectors.counting()));
	  return ma;
	 }
	


}
